// src/App.js
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table } from 'react-bootstrap';
import { Line } from 'react-chartjs-2';
import 'chartjs-adapter-date-fns'; // Import the date-fns adapter

import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

const App = () => {
  const [leaderboard, setLeaderboard] = useState([]);
  const [selectedPlayerData, setSelectedPlayerData] = useState([]);

  useEffect(() => {
    // Fetch leaderboard data from the server
    axios.get('http://localhost:5000/leaderboard').then((response) => {
      setLeaderboard(response.data);
    });
  }, []);

  const handlePlayerCellClick = (playerData) => {
    setSelectedPlayerData([playerData]);
  };

  return (
    <div className="container mt-4">
      <h1 className="mb-4">Leaderboard</h1>
      <div>
        <Table striped bordered hover responsive>
          <thead>
            <tr>
              <th>Name</th>
              <th>Total Kills</th>
              <th>Total Deaths</th>
              <th>Total Score</th>
            </tr>
          </thead>
          <tbody>
            {leaderboard.map((player) => (
              <tr key={player.name} onClick={() => handlePlayerCellClick(player)}>
                <td>{player.name}</td>
                <td>{player.totalKills}</td>
                <td>{player.totalDeaths}</td>
                <td>{player.totalScore}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      {selectedPlayerData.length > 0 && (
        <div className="mt-4">
          <h2>Player Data Chart</h2>
          <Line
            data={{
              labels: selectedPlayerData.map((data) => data.gamemode),
              datasets: [
                {
                  label: 'Kills',
                  data: selectedPlayerData.map((data) => data.totalKills), // Use 'totalKills' instead of 'kills'
                  borderColor: 'rgba(75, 192, 192, 1)',
                  backgroundColor: 'rgba(75, 192, 192, 0.2)',
                  borderWidth: 1,
                },
                {
                  label: 'Deaths',
                  data: selectedPlayerData.map((data) => data.totalDeaths), // Use 'totalDeaths' instead of 'deaths'
                  borderColor: 'rgba(255, 99, 132, 1)',
                  backgroundColor: 'rgba(255, 99, 132, 0.2)',
                  borderWidth: 1,
                },
                {
                  label: 'Score',
                  data: selectedPlayerData.map((data) => data.totalScore), // Use 'totalScore' instead of 'score'
                  borderColor: 'rgba(54, 162, 235, 1)',
                  backgroundColor: 'rgba(54, 162, 235, 0.2)',
                  borderWidth: 1,
                },
              ],
            }}
            options={{
              scales: {
                x: {
                  type: 'linear',
                  beginAtZero: true,
                },
                y: {
                  beginAtZero: true,
                },
              },
            }}
          />
        </div>
      )}
    </div>
  );
};

export default App;
