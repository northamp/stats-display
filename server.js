const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

// Connect to your MongoDB Atlas database
const uri = '-snip-';
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const connection = mongoose.connection;
connection.once('open', () => {
  console.log('MongoDB database connection established successfully');
});

// Define the schema for the leaderboard data
const leaderboardSchema = new mongoose.Schema({
  gamemode: String,
  map: String,
  modded: Boolean,
  players: [
    {
      deaths: Number,
      kills: Number,
      name: String,
      score: Number,
    },
  ],
  server_id: Number,
  timestamp: Date,
});

// Create a model for the leaderboard collection
const Leaderboard = mongoose.model('Leaderboard', leaderboardSchema, 'game-results');

// Define API routes
app.get('/leaderboard', async (req, res) => {
    try {
      const pipeline = [
        {
          $unwind: '$players',
        },
        {
          $group: {
            _id: '$players.name',
            totalKills: { $sum: '$players.kills' },
            totalDeaths: { $sum: '$players.deaths' },
            totalScore: { $sum: '$players.score' },
          },
        },
        {
          $project: {
            name: '$_id',
            totalKills: 1,
            totalDeaths: 1,
            totalScore: 1,
            _id: 0,
          },
        },
      ];
  
      const leaderboardData = await Leaderboard.aggregate(pipeline);
      res.json(leaderboardData);
    } catch (error) {
      res.status(500).json({ message: 'Error fetching leaderboard data' });
    }
  });

app.listen(port, () => {
  console.log(`Server is running on port: ${port}`);
});